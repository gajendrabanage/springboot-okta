package com.test.springbootokta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@SpringBootApplication
@EnableOAuth2Sso
//@EnableOAuth2Client 
public class SpringbootOktaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootOktaApplication.class, args);
	}
	
	


}
