package com.test.springbootokta.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	
	@GetMapping("/")
    public String home(Principal principle) {
        return "Welcome, "+ principle.getName()+ "!";
    }
	
	
	@GetMapping("/test")
    public String home(){
        return "Welcome";
    }
	
}
